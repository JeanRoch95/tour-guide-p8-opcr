# Utiliser une image de base Java
FROM maven:3.8.3-openjdk-17

# Copier le fichier pom.xml et les sources
COPY pom.xml /usr/src/app/
COPY src /usr/src/app/src

# Copiez les fichiers JAR locaux dans le répertoire temporaire de l'image
COPY libs/*.jar /usr/src/app/libs/

# Définir le répertoire de travail
WORKDIR /usr/src/app

# Installez les fichiers JAR dans le référentiel Maven local de l'image
RUN mvn install:install-file -Dfile=libs/gpsUtil.jar -DgroupId=gpsUtil -DartifactId=gpsUtil -Dversion=1.0.0 -Dpackaging=jar && \
    mvn install:install-file -Dfile=libs/RewardCentral.jar -DgroupId=rewardCentral -DartifactId=rewardCentral -Dversion=1.0.0 -Dpackaging=jar && \
    mvn install:install-file -Dfile=libs/TripPricer.jar -DgroupId=tripPricer -DartifactId=tripPricer -Dversion=1.0.0 -Dpackaging=jar

# Compiler l'application
RUN mvn clean package -DskipTests

# Exposer le port
EXPOSE 8080

# Commande pour exécuter l'application
CMD ["java", "-jar", "target/tourguide-0.0.1-SNAPSHOT.jar"]

