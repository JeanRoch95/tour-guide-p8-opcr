package com.openclassrooms.tourguide.tracker;

import com.openclassrooms.tourguide.service.TourGuideService;
import com.openclassrooms.tourguide.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

@Component
public class Tracker extends Thread {
	private Logger logger = LoggerFactory.getLogger(Tracker.class);
	private final TourGuideService tourGuideService;
	private final long trackingPollingInterval = TimeUnit.MINUTES.toSeconds(5);
	private volatile boolean stop = false;

	public Tracker(TourGuideService tourGuideService) {
		this.tourGuideService = tourGuideService;
	}

	@Override
	public void run() {
		while (!stop) {
			List<User> users = tourGuideService.getAllUsers();
			CompletableFuture<?>[] futures = users.stream()
					.map(tourGuideService::trackUserLocation)
					.toArray(CompletableFuture[]::new);

			CompletableFuture.allOf(futures).join();

			try {
				TimeUnit.SECONDS.sleep(trackingPollingInterval);
			} catch (InterruptedException e) {
				logger.error("Tracker interrupted", e);
				stop = true;
			}
		}
	}
}
